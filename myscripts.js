//------------ Variáveis globais ------------\\
let arrayDeCores = []
let verify = []
//--------------------------FIM DAS VARIAVEIS GLOBAIS---------------------------------------------------\\


//--------------------------INICIO DAS FUNÇÕES-----------------------------------------------------------\\
function createTable(){
    const selectedLevel = parseInt(document.querySelector('input[name="level"]:checked').value)
    const container = document.querySelector('#container')
    container.style.display = "grid"
    container.style.gap = `10px 16px`
    container.innerHTML = ""
    arrayDeCores = []
    switch(selectedLevel){
        case 1:
            container.style.gridTemplateColumns = "repeat(2, 1fr)"
            container.style.gridTemplateRows = "repeat(2, 1fr)"
            makeDivs(4)
            break
        case 2:
            container.style.gridTemplateColumns = "repeat(3, 1fr)"
            container.style.gridTemplateRows = "repeat(2, 1fr)"
            makeDivs(6)
            break
        case 3:
            container.style.gridTemplateColumns = "repeat(4, 1fr)"
            container.style.gridTemplateRows = "repeat(2, 1fr)"
            makeDivs(8)
            break
        case 4:
            container.style.gridTemplateColumns = "repeat(4, 1fr)"
            container.style.gridTemplateRows = "repeat(4, 1fr)"
            makeDivs(16)
            break
        case 5:
            container.style.gridTemplateColumns = "repeat(5, 1fr)"
            container.style.gridTemplateRows = "repeat(4, 1fr)"
            makeDivs(20)
            break
    }
}


function makeDivs(n){
    
    for(let i = 0; i < n; i++){
        let divCardFront = document.createElement('div')
        divCardFront.id = `${i}`
        divCardFront.classList.add('card')
        container.appendChild(divCardFront)
        divCardFront.addEventListener("click", changeColor)
    }
    let respostas = []
    let depositoDeCores = ["#00FFFF", "#FFFF00", "#9966CC", "#0000FF", "#4682B4", "#F400A1", "#6B4423", "#FF8C00", "#000000", "#00CED1"]
    let divNodeList = document.querySelectorAll('.card')
    let divArray = Array.from(divNodeList)
    for(let i = 0; i <= (divArray.length -1) / 2; i++){
        arrayDeCores.push(depositoDeCores[i])
        arrayDeCores.push(depositoDeCores[i])
    }
    shuffleArray(arrayDeCores)
    // shuffleArray(arrayDeCores)

    
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }


}

function changeColor(){
    let aviso1 = document.querySelector('#aviso1')
    let aviso2= document.querySelector('#aviso2')
    if (verify.length > 1){
        aviso2.style.display = "flex"
        verify[0].style.backgroundColor = ""
        verify[1].style.backgroundColor = ""
        verify = []
    } else if ( verify.length === 1){
        this.style.backgroundColor = arrayDeCores[this.id]
        verify.push(this)
        if (verify[0].id == verify[1].id){
            aviso1.style.display = "flex"
            verify[0].style.backgroundColor = ""
            verify[1].style.backgroundColor = ""
            verify = []
        } else if(verify[0].style.backgroundColor === verify[1].style.backgroundColor && verify[0].id !== verify[1].id){
            setTimeout( () => {
                verify[0].style.opacity = "0"
                verify[1].style.opacity = "0"
                verify = []
            }, 800)
        } else if (verify[0].style.backgroundColor !== verify[1].style.backgroundColor){
            setTimeout(() => {
                verify[0].style.backgroundColor = ""
                verify[1].style.backgroundColor = ""
                verify = []
            }, 800)
        } 
        
    } else if (verify == 0){
        this.style.backgroundColor = arrayDeCores[this.id]
        verify.push(this)
    }
    
}

function closeWarning(){
    let aviso = document.querySelectorAll('.aviso')
    aviso[0].style.display = ""
    aviso[1].style.display = ""
}
// -------------------------------------------------Termino das funções-------------------------------------